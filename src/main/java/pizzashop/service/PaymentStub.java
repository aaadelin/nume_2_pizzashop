package pizzashop.service;

import pizzashop.model.Payment;
import pizzashop.model.PaymentType;

import java.util.Objects;

public class PaymentStub extends Payment {

    private int tableNumber = 1;
    private PaymentType type = PaymentType.Card;
    private double amount = 2;

    public PaymentStub(int tableNumber, PaymentType type, double amount) {
        super(tableNumber, type, amount);
    }

    public PaymentStub() {
        super(1, PaymentType.Card, 2);
    }

    public int getTableNumber() {
        return this.tableNumber;
    }

    public PaymentType getType() {
        return this.type;
    }

    public double getAmount() {
        return this.amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!super.equals(o)) return false;
        PaymentStub that = (PaymentStub) o;
        return tableNumber == that.tableNumber &&
                Double.compare(that.amount, amount) == 0 &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), tableNumber, type, amount);
    }
}
