package service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.PaymentRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PaymentRepositoryUnitTestMock {

    PaymentRepository paymentRepository;

    @BeforeEach
    void setUp() {
    }

    @Test
    public void testWriteAll(){
        paymentRepository = Mockito.spy(PaymentRepository.class);
        Payment payment = new Payment(1, PaymentType.Card, -3);
        List<Payment> allPayments = paymentRepository.getAll();
        allPayments.add(payment);

        Mockito.doAnswer((invocationOnMock -> {
            Object arg0 = invocationOnMock.getArguments()[0];
            assertEquals(allPayments, arg0);
            return null;
        })).when(paymentRepository).writeAll(Mockito.anyList());

        paymentRepository.add(payment);

    }

    @Test
    public void testGetAll(){
        paymentRepository = Mockito.spy(PaymentRepository.class);
        Payment payment = new Payment(1, PaymentType.Card, -3);
        paymentRepository.removeAllPayments();

        Mockito.doReturn(payment).when(paymentRepository).getPayment(Mockito.any());

        paymentRepository.readPayments();

        paymentRepository.getAll().forEach(item -> assertEquals(payment, item));
    }


}