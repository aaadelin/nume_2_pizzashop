package service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PizzaService;
import pizzashop.validation.PositiveExpectedNumberException;
import pizzashop.validation.TableNotExistsException;

import java.util.List;

import static org.mockito.Mockito.times;

class PizzaServiceUnitTestMock {

    private PaymentRepository paymentRepository;
    private MenuRepository menuRepository;
    private PizzaService pizzaService;

    @BeforeEach
    void setUp() {

        paymentRepository = Mockito.mock(PaymentRepository.class);
        menuRepository = Mockito.mock(MenuRepository.class);
        pizzaService = new PizzaService(menuRepository, paymentRepository);

    }


    @Test
    public void testAddValidInput() throws PositiveExpectedNumberException, TableNotExistsException {
        pizzaService.addPayment(1, PaymentType.Card, 5);

        Payment payment = new Payment(1, PaymentType.Card, 5);
        Mockito.doNothing().when(paymentRepository).add(payment);

        Mockito.verify(paymentRepository, times(1)).add(payment);
    }

    @Test
        public void testAddAmountInvalid(){
        Payment payment = new Payment(1, PaymentType.Card, 5);

        Mockito.doReturn(List.of(payment)).when(paymentRepository).getAll();
        List<Payment> paymentList = paymentRepository.getAll();

        Mockito.verify(paymentRepository, times(1)).getAll();
        assert paymentList.equals(List.of(payment));
    }

}