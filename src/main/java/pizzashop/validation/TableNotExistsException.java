package pizzashop.validation;

public class TableNotExistsException extends Exception {
    private String message;

    public TableNotExistsException(String s) {
        this.message = s;
    }

    public String getMessage(){
        return message;
    }
}
