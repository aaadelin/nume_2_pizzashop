package service;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PizzaService;
import pizzashop.validation.PositiveExpectedNumberException;
import pizzashop.validation.TableNotExistsException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PizzaServiceTest {

    private static PizzaService pizzaService;

    @BeforeAll
    static void beforeAll() {
        MenuRepository menuRepository = new MenuRepository();
        PaymentRepository paymentRepository = new PaymentRepository();
        pizzaService = new PizzaService(menuRepository, paymentRepository);
    }

    @Tag("Should Pass")
    @Test
    void addPaymentWithRightParameters() {
        try {
            pizzaService.addPayment(1, PaymentType.Card, 100);
        } catch (TableNotExistsException | PositiveExpectedNumberException e) {
            assert false;
        }
    }

    @DisplayName("Testing negative amount")
    @Test
    void addPaymentWithInvalidAmount() {
        Exception exception = Assertions.assertThrows(PositiveExpectedNumberException.class, () ->
                pizzaService.addPayment(2, PaymentType.Card, -1)
        );

        String expectedMessage = "Only positive numbers!";
        assertEquals(expectedMessage, exception.getMessage());
    }

//    @RepeatedTest(2)
    @Test
    void addPaymentWithInvalidTableNumber() {
        Exception exception = Assertions.assertThrows(TableNotExistsException.class, () ->
                pizzaService.addPayment(0, PaymentType.Card, 5)
        );

        String expectedMessage = "Available tables are from 1 to 8!";
        assertEquals(expectedMessage, exception.getMessage());
    }

//    @ParameterizedTest
//    @ValueSource(ints = {9, 10, 11, 12})
    @Test
    void addPaymentWithNegativeAmount() {
        Exception exception = Assertions.assertThrows(TableNotExistsException.class, () ->
                pizzaService.addPayment(10, PaymentType.Card, -5)
        );

        String expectedMessage = "Available tables are from 1 to 8!";
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    void addPaymentWithFailingBoundaryTableNumber() {
        Exception exception = Assertions.assertThrows(TableNotExistsException.class, () ->
                pizzaService.addPayment(0, PaymentType.Card, 10)
        );

        String expectedMessage = "Available tables are from 1 to 8!";
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    void addPaymentWithPassingBoundaryFailingNumber() {
        try {
            pizzaService.addPayment(1, PaymentType.Card, 100);
        } catch (TableNotExistsException | PositiveExpectedNumberException e) {
            assert false;
        }
    }

    @Test
    void addPaymentWithFailingAmountBoundary() {
        Exception exception = Assertions.assertThrows(PositiveExpectedNumberException.class, () ->
                pizzaService.addPayment(4, PaymentType.Card, 0)
        );

        String expectedMessage = "Only positive numbers!";
        assertEquals(expectedMessage, exception.getMessage());
    }

//    @Disabled
    @Test
    void addPaymentWithPassingAmountBoundary() {
        try {
            pizzaService.addPayment(4, PaymentType.Card, 0.1);
        } catch (TableNotExistsException | PositiveExpectedNumberException e) {
            assert false;
        }
    }


}