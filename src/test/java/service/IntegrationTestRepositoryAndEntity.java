package service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PizzaService;
import pizzashop.validation.TableNotExistsException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IntegrationTestRepositoryAndEntity {
    private PizzaService service;
    private MenuRepository menuRepository;
    private PaymentRepository paymentRepository;

    @BeforeEach
    void setUp(){
        menuRepository = new MenuRepository();
        paymentRepository = new PaymentRepository();
        service = new PizzaService(menuRepository, paymentRepository);
    }

    @Test
    public void testGetTotalAmount(){
        Double sum = service.getPayments().stream().filter(payment -> payment.getType()==PaymentType.Card)
                .map(Payment::getAmount)
                .reduce((double) 0, Double::sum);
        assert service.getTotalAmount(PaymentType.Card) == sum;
    }

    @Test
    public void testAddPayment() {
        Exception exception = Assertions.assertThrows(TableNotExistsException.class, () ->
                service.addPayment(100, PaymentType.Card, 5)
        );

        String expectedMessage = "Available tables are from 1 to 8!";
        assertEquals(expectedMessage, exception.getMessage());
    }
}
