package pizzashop.validation;

public class PositiveExpectedNumberException extends Exception {
    private String message;

    public PositiveExpectedNumberException(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }
}
