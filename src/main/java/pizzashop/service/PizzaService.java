package pizzashop.service;

import pizzashop.model.MenuDataModel;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.validation.PositiveExpectedNumberException;
import pizzashop.validation.TableNotExistsException;

import java.util.List;

public class PizzaService {

    private MenuRepository menuRepo;
    private PaymentRepository payRepo;

    public PizzaService(MenuRepository menuRepo, PaymentRepository payRepo) {
        this.menuRepo = menuRepo;
        this.payRepo = payRepo;
    }

    public List<MenuDataModel> getMenuData() throws Exception {
        return menuRepo.getMenu();
    }

    public List<Payment> getPayments() {
        return payRepo.getAll();
    }

    public void addPayment(int table, PaymentType type, double amount) throws TableNotExistsException, PositiveExpectedNumberException {
        if (table <= 0 || table >= 9) {
            throw new TableNotExistsException("Available tables are from 1 to 8!");
        }
        if (amount <= 0) {
            throw new PositiveExpectedNumberException("Only positive numbers!");
        }
        Payment payment = new Payment(table, type, amount);
        payRepo.add(payment);
    }

    public double getTotalAmountWithList(PaymentType type, List<Payment> payments){
        double total = 0.0f;
        if (payments == null) {
            throw new IllegalArgumentException("No null list!");
        }else if (payments.size() == 0) {
            return total;
        }
        for (Payment p : payments) {
            if (p.getType().equals(type))
                total += p.getAmount();
        }
        return total;
    }

    public double getTotalAmount(PaymentType type) {
        return getTotalAmountWithList(type, getPayments());
    }

}
