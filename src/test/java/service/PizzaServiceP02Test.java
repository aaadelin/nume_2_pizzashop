package service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PizzaService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PizzaServiceP02Test {
    private static PizzaService pizzaService;

    @BeforeAll
    static void beforeAll() {
        MenuRepository menuRepository = new MenuRepository();
        PaymentRepository paymentRepository = new PaymentRepository();
        pizzaService = new PizzaService(menuRepository, paymentRepository);
    }

    @Test
    void calculateTotalAmountWithNullList(){
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                pizzaService.getTotalAmountWithList(PaymentType.Cash, null)
        );
    }

    @Test
    void calculateTotalAmountWithCashAndCorrectList(){
        List<Payment> payments = new ArrayList<>(); payments.add(new Payment(1, PaymentType.Cash, 1));
        double amount = pizzaService.getTotalAmountWithList(PaymentType.Cash, payments);

        assertEquals(amount, 1);
    }

}
