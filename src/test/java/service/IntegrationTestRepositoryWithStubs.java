package service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PaymentStub;
import pizzashop.service.PizzaService;
import pizzashop.validation.PositiveExpectedNumberException;
import pizzashop.validation.TableNotExistsException;

import java.util.ArrayList;
import java.util.List;

public class IntegrationTestRepositoryWithStubs {

    private PizzaService service;
    private MenuRepository menuRepository;
    private PaymentRepository paymentRepository;

    @BeforeEach
    void setUp(){
        Payment payment = new PaymentStub();
        Payment payment1 = new PaymentStub();

        List<Payment> payments = new ArrayList<>();
        payments.add(payment); payments.add(payment1);
        menuRepository = new MenuRepository();
        paymentRepository = new PaymentRepository(payments);
        service = new PizzaService(menuRepository, paymentRepository);
    }

    @Test
    public void testGetTotalAmount(){
        assert service.getTotalAmount(PaymentType.Card) == 4;
    }

    @Test
    public void testAddPayment() throws PositiveExpectedNumberException, TableNotExistsException {
        Payment payment = new PaymentStub();
        service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());

        List<Payment> payments = service.getPayments();
        assert payments.get(payments.size()-1).equals(payment);
    }

}
